// soal 1
const hitungLuasPersegi = (p, l) => p * l;
const hitungKelilingPersegi = (p, l) => 2 * (p + l);

console.log(hitungLuasPersegi(2, 2));
console.log(hitungKelilingPersegi(18, 7));

// soal 2
const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
    },
  };
};

newFunction("William", "Imoh").fullName();

// soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

let { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

// soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(before);
