// soal pertama
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(
  pertama.substring(0, 5) +
    pertama.substring(12, 19) +
    kedua.substring(0, 7) +
    kedua.substring(7, 18).toUpperCase()
);

// soal kedua
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

console.log(
  (Number(kataKedua) + Number(kataKetiga)) *
    (Number(kataPertama) - Number(kataKeempat))
);

// soal ketiga
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(3, 14);
var kataKetiga = kalimat.substring(14, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(24, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
