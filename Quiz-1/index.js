// soal 1

function next_date(tanggal, bulan, tahun) {
  if ((0 == tahun % 4 && 0 != tahun % 100) || 0 == tahun % 400) {
    if (tanggal == 31 && bulan == 12) {
      tanggal = 1;
      bulan = 1;
      tahun = tahun + 1;
    } else if (tanggal == 29 && bulan == 2) {
      tanggal = 1;
      bulan = bulan + 1;
    } else if (
      (tanggal == 30 && bulan == 4) ||
      bulan == 6 ||
      bulan == 9 ||
      bulan == 11
    ) {
      tanggal = 1;
      bulan = bulan + 1;
    } else {
      tanggal = tanggal + 1;
    }
  } else {
    if (tanggal == 31 && bulan == 12) {
      tanggal = 1;
      bulan = 1;
      tahun = tahun + 1;
    } else if (tanggal == 28 && bulan == 2) {
      tanggal = 1;
      bulan = bulan + 1;
    } else if (
      (tanggal == 30 && bulan == 4) ||
      bulan == 6 ||
      bulan == 9 ||
      bulan == 11
    ) {
      tanggal = 1;
      bulan = bulan + 1;
    } else {
      tanggal = tanggal + 1;
    }
  }

  return tanggal + " " + bulan + " " + tahun;
}

// soal 2

function jumlah_kata(string) {
  var stringTrim = string.trim();
  var splitString = stringTrim.split(" ");
  return splitString.length;
}
