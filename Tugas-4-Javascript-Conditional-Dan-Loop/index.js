// soal 1
var nilai = 40;

if (nilai >= 85) {
  console.log("A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("D");
} else {
  console.log("E");
}

// soal 2
var tanggal = 21;
var bulan = 1;
var tahun = 1998;

switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }
}

console.log(tanggal + " " + bulan + " " + tahun);

// soal 3
var n = 7;

// variabel pembantu
var pagar = "";

for (var i = 0; i <= n; i++) {
  for (var j = 0; j < i; j++) {
    pagar = pagar + "#";
  }
  console.log(pagar);
  pagar = "";
}

// soal 4
var m = 10;

// variabel pembantu
var i = 1;
var pertama = false;
var kedua = false;
var ketiga = false;
var pagar = "###";

do {
  if (pertama == false) {
    console.log(i + " - I Love Programming");
    pertama = true;
  } else if (kedua == false) {
    console.log(i + " - I Love Javascript");
    kedua = true;
  } else {
    console.log(i + " - I Love VueJS");
    ketiga = true;
  }
  if (pertama && kedua && ketiga) {
    console.log(pagar);
    pagar = pagar + "###";
    pertama = false;
    kedua = false;
    ketiga = false;
  }

  i++;
} while (i <= m);
