// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewanSort = daftarHewan.sort();
daftarHewanSort.forEach(function (hewan) {
  console.log(hewan);
});

// soal 2

function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun, alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3

function hitung_huruf_vokal(nama) {
  //variabel pemnantu
  var hurufVokal = ["a", "i", "u", "e", "o"];
  var jumlahHurufVokal = 0;
  var kataKecil = nama.toLowerCase();
  var pecahKata = kataKecil.split("");

  pecahKata.forEach(function (kata) {
    hurufVokal.forEach(function (vokal) {
      if (kata === vokal) {
        jumlahHurufVokal = jumlahHurufVokal + 1;
      }
    });
  });
  return jumlahHurufVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

// soal 4

function hitung(angka) {
  var deret = -2;
  for (var i = 0; i < angka; i++) {
    if (angka < 0) {
      return deret;
    } else {
      deret = deret + 2;
    }
  }

  return deret;
}

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
