var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

const readBooks = async () => {
  bookOne = await readBooksPromise(10000, books[0]);
  bookTwo = await readBooksPromise(bookOne, books[1]);
  bookThree = await readBooksPromise(bookTwo, books[2]);
  bookFour = await readBooksPromise(bookThree, books[3]);
};

readBooks();
